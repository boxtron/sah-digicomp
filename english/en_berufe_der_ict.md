<!---
Create: 08.2019
Title: index
--->
# Berufe der ICT: Hauptaufgaben und Kurzbeschreibungen

Quelle:  
swissICT. (2017).  
Berufe der ICT: Branchenübliche ICT- und Organisations-Berufsbilder mit erforderlichen Kompetenzen und Ausbildungen (9. überarbeitete und aktualisierte Auflage).  
Zürich: vdf Hochschulverlag AG an der ETH Zürich.

[<img src="/pic/swissICT-berufe_ict.png" width="200" border="" />](https://www.berufe-der-ict.ch/berufe)  

---


| Rubrik | Deutsch | English
|------- |---------|---------------------------
| Gesamtleitung (CIO) | [ICT oder ICT und Organisation](http://www.berufe-der-ict.ch/berufe/gesamtleitung-ict-oder-ict-und-organisation) | [Chief Information Officer (CIO)](#Chief-Information-Officer-(CIO))
| Gesamtleitung (Plan) | [Planung, Steuerung und Beratung](http://www.berufe-der-ict.ch/berufe/planung/gesamtleitung-planung-steuerung-und-beratung) | [Head Plan](#Head-Plan)
| Gesamtleitung (Build) | [Design, Engineering, Bereitstellung und Implementierung](http://www.berufe-der-ict.ch/berufe/entwicklung/gesamtleitung-design-engineering-bereitstellung-und-implementierung) | [Head Build](#head-build)
| Gesamtleitung (Run) | [Betrieb, Administration und Unterstützung](http://www.berufe-der-ict.ch/berufe/betrieb/gesamtleitung-betrieb-administration-und-unterstuetzung) | [Head Run](#head-run)
| Gesamtleitung (PM) | [Projektmanagement](http://www.berufe-der-ict.ch/berufe/projektmanagement/gesamtleitung-projektmanagement) | [Head Project Management](#head-project-management)
| Gesamtleitung (ORG) | [Organisation](http://www.berufe-der-ict.ch/berufe/organisation-und-betriebswirtschaft/gesamtleitung-organisation) | [Head Organization and Business Administration](#head-organization-and-business-administration)
| Plan | [ICT-Architekt](http://www.berufe-der-ict.ch/berufe/planung/ict-architekt-in) | [ICT Architect](#ict-architect)
| | [ICT-Auditor](http://www.berufe-der-ict.ch/berufe/planung/ict-auditor-in) | [ICT Auditor](#ict-auditor)
| | [ICT-Berater](http://www.berufe-der-ict.ch/berufe/planung/ict-berater-in) | [ICT Consultant](#ict-consultant)
| | [ICT-Controller](http://www.berufe-der-ict.ch/berufe/planung/ict-controller-in) | [ICT Controller](#ict-controller)
| | [ICT-Qualitätsmanager](http://www.berufe-der-ict.ch/berufe/planung/ict-qualitaetsmanager-in) | [ICT Quality Manager](#ict-quality-manager)
| | [ICT-Sicherheitsbeauftragter](http://www.berufe-der-ict.ch/berufe/planung/ict-sicherheitsbeauftragter-in) | [ICT Security Officer](#ict-security-officer)
| | [ICT-Sourcing-Manager](http://www.berufe-der-ict.ch/berufe/planung/ict-sourcing-manager-in) | [ICT Sourcing Manager](#ict-sourcing-manager)
| Build | [Applikations-Entwickler](http://www.berufe-der-ict.ch/berufe/entwicklung/applikations-entwickler-in) | [Application Engineer](#application-engineer)
| | [Data-Scientist](http://www.berufe-der-ict.ch/berufe/entwicklung/data-scientist-in) | [Data Scientist](#data-scientist)
| | [Datenbank-Spezialist](http://www.berufe-der-ict.ch/berufe/entwicklung/datenbank-spezialist-in) | [Database Specialist](#database-specialist)
| | [ICT-Requirements-Engineer](http://www.berufe-der-ict.ch/berufe/entwicklung/ict-requirements-engineer-in) | [ICT Requirements Engineer](#ict-requirements-engineer)
| | [ICT-Security-Spezialist](http://www.berufe-der-ict.ch/berufe/entwicklung/ict-security-spezialist-in) | [ICT Security Specialist](#ict-security-specialist)
| | [ICT-System-Ingenieur](http://www.berufe-der-ict.ch/berufe/entwicklung/ict-system-ingenieur-in) | [ICT System Engineer](#ict-system-engineer)
| | [ICT-Test-Ingenieur](http://www.berufe-der-ict.ch/berufe/entwicklung/ict-test-ingenieur-in) | [ICT Test Engineer](#ict-test-engineer)
| | [ICT-Testmanager](http://www.berufe-der-ict.ch/berufe/entwicklung/ict-testmanager-in) | [ICT Test Manager](#ict-test-manager)
| | [Netzwerk-Spezialist](http://www.berufe-der-ict.ch/berufe/entwicklung/netzwerk-spezialist-in) | [Network Specialist](#network-specialist)
| | [Software-Ingenieur](http://www.berufe-der-ict.ch/berufe/entwicklung/software-ingenieur-in) | [Software Engineer](#software-engineer)
| | [User-Experience-Architekt](http://www.berufe-der-ict.ch/berufe/entwicklung/user-experience-architekt-in) | [ICT User Experience Architect](#ict-user-experience-architect)
| | [Wirtschaftsinformatiker](http://www.berufe-der-ict.ch/berufe/entwicklung/wirtschaftsinformatiker-in) | [IS Engineer (IT Solution Engineer)](#is-engineer-it-solution-engineer)
| Run | [Applikations-Manager](http://www.berufe-der-ict.ch/berufe/betrieb/applikations-manager-in) | [Application Manager](#application-manager)
| | [Datenbank Administrator](http://www.berufe-der-ict.ch/berufe/betrieb/datenbank-administrator-in) | [Database Administrator](#database-administrator)
| | [ICT-Change-Manager](http://www.berufe-der-ict.ch/berufe/betrieb/ict-change-manager-in) | [ICT Change Manager](#ict-change-manager)
| | [ICT-Helpdesk-Mitarbeiter](http://www.berufe-der-ict.ch/berufe/betrieb/ict-helpdesk-mitarbeiter-in) | [ICT Helpdesk Employee](#ict-helpdesk-employee)
| | [ICT-Operator](http://www.berufe-der-ict.ch/berufe/betrieb/ict-operator-in) | [ICT Operator](#ict-operator)
| | [ICT-Produktionsplaner](http://www.berufe-der-ict.ch/berufe/betrieb/ict-produktionsplaner-in) | [ICT Production Planner](#ict-production-planner)
| | [ICT-Security-Operations-Manager](http://www.berufe-der-ict.ch/berufe/betrieb/ict-security-operations-manager-in) | [ICT Security Operations Manager](#ict-security-operations-manager)
| | [ICT-Service-Manager](http://www.berufe-der-ict.ch/berufe/betrieb/ict-service-manager-in) | [ICT Service Manager](#ict-service-manager)
| | [ICT-Supporter](http://www.berufe-der-ict.ch/berufe/betrieb/ict-supporter-in) | [ICT Supporter](#ict-supporter)
| | [ICT-System-Administrator](http://www.berufe-der-ict.ch/berufe/betrieb/ict-system-administrator-in) | [ICT System Administrator](#ict-system-administrator)
| | [ICT System-Controller](http://www.berufe-der-ict.ch/berufe/betrieb/ict-system-controller-in) | [ICT System Controller](#ict-system-controller)
| | [ICT-System-Spezialist](http://www.berufe-der-ict.ch/berufe/betrieb/ict-system-spezialist-in) | [ICT System Specialist](#ict-system-specialist)
| | [ICT-Techniker](http://www.berufe-der-ict.ch/berufe/betrieb/ict-techniker-in) | [ICT Technician](#ict-technician)
| | [Netzwerk-Administrator](http://www.berufe-der-ict.ch/berufe/betrieb/netzwerk-administrator-in) | [Network Administrator](#network-administrator)
| PM | [Projektmanagement-Officer](http://www.berufe-der-ict.ch/berufe/projektmanagement/projektmanagement-officer-in) | [Project Management Officer](#project-management-officer)
| | [Projektleiter](http://www.berufe-der-ict.ch/berufe/projektmanagement/projektleiter-in) | [Project Leader](#project-leader)
| | [Program-Manager](http://www.berufe-der-ict.ch/berufe/projektmanagement/programm-manager-in) | [Program Manager](#program-manager)
| Org | [Business-Analyst](http://www.berufe-der-ict.ch/berufe/organisation-und-betriebswirtschaft/business-analyst-in) | [Business Analyst](#business-analyst)
| | [Prozess-Manager](http://www.berufe-der-ict.ch/berufe/organisation-und-betriebswirtschaft/prozess-manager-in) | [Process Manager](#process-manager)
| | [Unternehmensorganisator](http://www.berufe-der-ict.ch/berufe/organisation-und-betriebswirtschaft/unternehmensorganisator-in) | [Business Organization Consultat](#business-organization-consultat)
| | [Organisations-Manager](http://www.berufe-der-ict.ch/berufe/organisation-und-betriebswirtschaft/organisations-manager-in) | [Organization Manager](#organization-manager)
| New | [DevOps Engineer](http://www.berufe-der-ict.ch/berufe/methodikbezogene-berufe/devops-engineer) | [DevOps Engineer](#devops-engineer)
| | [Product Owner](http://www.berufe-der-ict.ch/berufe/methodikbezogene-berufe/product-owner) | [Product Owner](#product-owner)
| | [Scrum Master](http://www.berufe-der-ict.ch/berufe/methodikbezogene-berufe/scrum-master) | [Scrum Master](#scrum-master)


---
## Chief Information Officer (CIO)

Brief description  
Overall management and leadership of information and communication technologies and the organisation of a company

Responsible for:
* The formulation of the ICT vision and ICT strategy for the Company, derived from the business strategy of the executive board
* Ensuring ICT Governance
* Ensuring ICT operation, ICT security and the further development of ICT systems
* The ICT services required to optimally support business processes


---
## Head Plan

Brief description  
Overall management and leadership of the professional group: planning, control and consulting

Responsible for:
* The conception of ICT architectures and ICT services derived from the company's ICT strategy
* Strategic planning, quality strategy, quality management and security in the ICT area
* Checking the quality, security and compliance of ICT systems
* The management of ICT risks
* The creation of the sourcing strategy derived from the ICT strategy of the company
* The procurement of IT resources and services
* Providing the necessary information on ICT governance


---
## Head Build

Brief description  
Overall management and leadership of the professional group: design, engineering, provision and implementation

Responsible for:
* Engineering, design, creation, testing and implementation of system, software, database and telecommunications solutions
* Procurement of ICT systems
* Integration of standard solutions
* Providing ICT services in accordance with the ICT strategy and ICT architectures


---
## Head Run

Brief description  
Overall management and leadership of the professional group: operations, administration and support

Responsible for:
* The infrastructure in the ICT operations environment and for production
* Operation of the  hardware: host, server, network, peripheral devices, workstation systems
* Operation of the software: operation, communication, database and application software.
* Operation of the computer centre, decentralised systems and the production service desk
* The configuration/change management and its processes in the environment of ICT operations
* Compliance with Service Level Agreements
* The integration of services in the area of ICT operations from external service providers
* The guarantee of operational safety


---
## Head Project Management

Brief description  
Leading the project management

Responsible for:
* Prioritization, coordination, execution and monitoring of projects and programs in the ICT and organizational area
* Project portfolio management


---
## Head Organization and Business Administration

Brief description  
Leading the Organization Department

Responsible for:
* Strategy, organization, resource deployment, project management and business administration
* The design of business processes
* Process management and process optimization
* Definition and assignment of roles and functions
* Ensuring quality, risk and cost management


---
## ICT Architect

Brief description  
Planning and designing ICT architectures with different characteristics (e.g. on premise, from the cloud, service-based) at company, process, service and information level as well as for ICT systems (hardware, software, data, interfaces, communication networks), taking into account cost-effectiveness, quality and feasibility

* I can analyze the corporate strategy and advise the management on the definition of the ICT strategy.
* I can understand the needs and expectations of clients with regard to ICT systems and architectures; I can develop proposals for solutions and bases for decisions based on the requirements identified
* I can develop and update ICT architectures in relation to the ICT strategy, taking into account the company's objectives and economic efficiency.
* I can develop a deployment concept for hardware, software and communication and keep it up to date.
* I can explain the ICT strategy and ICT architectures in a way that is comprehensible to all and represent them both internally and externally.


---
## ICT Auditor

Brief description  
Assessment of risks and controls in the area of design and operation of ICT resources; assessment of the ICT organizational structure of a company in relation to: Management, leadership, control and the ICT resources used; checking the quality, security and regularity of existing and developing ICT systems (applications, processes, data, services and infrastructure); checking the internal control system (ICS) in ICT; assessing monitoring to ensure that legal and regulatory requirements and internal guidelines are met

* I can develop and implement a risk-related ICT audit strategy; I can plan and execute ICT audits based on the ICT audit standards, guidelines and practices defined in the audit strategy.
* I can assess the impact of vulnerabilities on the achievement of audit objectives; I can make recommendations on how to address these vulnerabilities.
* I can assess ICT development; commissioning and further development processes (applications, services and infrastructure) including project management and control mechanisms.
* I can assess risks of outsourced business units including their monitoring.
* I can perform software testing to certify functionality.


---
## ICT Consultant

Brief description  
Advising and supporting management and project managers in the development of ICT strategies and solutions with regard to requirements analysis, target definition, concept development and implementation

* I can acquire and execute ICT consulting projects and ICT customer projects
* I can advise stakeholders on the development of ICT strategies with short- and long-term requirements. I can design ICT solutions for stakeholders taking into account the goals, regulations, guidelines and guidelines for the use of ICT.
* I can evaluate ICT systems from customers, competitors and my own company with regard to technical and economic aspects, taking into account current trends and security aspects.
* I can prepare requirement analyses, requirement specifications and tender documents
* I can implement the planning of ICT solutions for customers, including controlling.


---
## ICT Controller

Brief description  
Supporting decision-making bodies in ICT projects in questions of project management, cost-effectiveness, quality and resources in projects and operations

* I can manage and control ICT projects; I can develop, implement and maintain controlling concepts
* I can define planning methods for the verification of profitability, quality and safety and can provide controlling instruments.
* I can analyze and review projects, budgets and investments with regard to corporate goals, success factors, follow-up costs and corporate resources.
* I can prepare, analyze and plausibilise data for reporting strategic and operational ICT planning; I can support management with projections and budget planning
* I can create bottleneck and deviation analyses and can show the consequences of changed ICT requirements.


---
## ICT Quality Manager

Brief description  
Developing or further developing and introducing an ICT quality strategy geared to business objectives; establishing an effective quality management system; planning, designing, implementing and measuring quality measures to ensure product/service quality; advising, accompanying and supporting project managers and line managers on all ICT quality management issues and implementing quality guidelines

* I can analyze the corporate strategy and vision and derive suitable sustainable ICT quality strategies to ensure the quality of processes, data, services and ICT systems.
* I can design an ICT quality management system with methods, guidelines, standards, processes and measures for quality control.
* I can develop and implement quality specifications, principles and guidelines and I can check their compliance.
* I can measure the goal achievement of quality management systems
* I can support and accompany project and line units in the implementation of quality measures (e.g. Quality Plan) as well as in the achievement of set quality goals.


---
## ICT Security Officer

Brief description  
Defining ICT security objectives and developing, implementing and updating ICT security strategies to ensure the confidentiality, integrity, availability and security of information; identifying and assessing information risks arising from threat and vulnerability analyses; proposing and enforcing security measures; advising and supporting employees on all information security issues; and ensuring reporting

* I can develop and implement an information security strategy based on the business objectives, taking into account the security requirements
* I can identify real and potential, legal and regulatory requirements and assess their impact on information security
* I can establish escalation and communication procedures for information security incidents; I can integrate these procedures into the business continuity plan
* I can implement a systematic and structured ICT risk management process
* I can monitor and support information security activities in all business processes (e.g. development, procurement, hiring)


---
## ICT Sourcing Manager

Brief description  
Leading the sourcing process throughout the entire lifecycle of ICT products and/or ICT services; creating the sourcing strategy taking business aspects into account; advising and supporting stakeholders

* I can design sourcing strategies taking into account the corporate strategy and the sourcing risks
* I can identify, review and document the needs of stakeholders; I can advise and support them by working with them to create needs-based requirement specifications 
* I can define the supplier policy; I can coordinate supplier, consultant and partner activities; I can assess suppliers and establish communication and escalation procedures in case of problems with suppliers.
* I can conduct contract negotiations, adapt and prepare contracts and ensure their compliance.
* I can identify, track and evaluate risks in the sourcing area; I can initiate and implement the necessary measures to reduce risks and check their effectiveness


---
## Application Engineer

Brief description  
Developing and maintaining software applications based on business and operational requirements and ensuring implementation

* I can analyze problems in the area of business processes/transactions, data and functions as well as in the environment (workplace, target systems/platforms, framework organization).  
* I can create models for data, functions and/or objects
* I can develop and maintain software applications, components, modules and interfaces to databases as well as to other applications to ensure the implementation and maintenance of this software.
* I can assist in the implementation and testing (integration testing) of software applications
* I can design and implement software improvements in the lifecycle of applications


---
## Data Scientist

Brief description  
Execution of predictive and complex statistical data analyses based on large unstructured and structured data sets (big data) and, if necessary, a company-wide data architecture; evaluation or development of ICT analysis infrastructures and analysis tools

* I can merge and deploy structured and unstructured datasets with different data types and formats and from different data sources for creating explorative data analyses taking into account data sensitivity; I can make these datasets available for integration into analysis and information systems; I can build a test environment, create and run tests.
* I can identify and analyze relevant patterns and relationships in data: I can create and visualize data analyses using modern methods of data mining and machine learning to derive decision bases and recommendations for action for strategic and operational decisions.
* I can design, build and integrate automated data analysis into ICT processes; I can develop scalable forecasting and expert systems using analytical tools.
* I can create data, function and/or object models; I can develop, implement and maintain interfaces to databases and other applications.
* I can develop and apply complex algorithms (e.g. clustering, linear regression, neural networks and decision trees) for determining cause-effect relationships and for creating quantitative forecast models.


---
## Database Specialist

Brief description  
Planning and processing of all tasks in the area of the use of databases; elaboration and realization of system concepts including cross-platform distributed databases

* I can plan and monitor database usage; I can define the necessary system requirements to ensure optimal database usage
* I can support and advise system and application developers on logical design, database technology and database operation.
* I can describe and classify data at a technical level.
* I can specify, implement and maintain databases and assign access rights to them
* I can create migration plans when I upgrade database releases or migrate to another database software.


---
## ICT Requirements Engineer

Brief description  
Recording, analyzing, validating, consolidating, communicating and documenting the needs and expectations of clients for solutions using ICT technologies; ensuring the alignment of stakeholder requirements; modeling business processes and analyzing the resulting requirements for ICT systems; working out the requirements for ICT solutions from the aspects of cost-effectiveness and feasibility

* I can analyse the overall context of the software solution to be created in terms of processes, structures, data and user interfaces.
* I can capture, analyze, validate and document the needs and expectations of clients for ICT solutions using standardized procedures.
* I can create functional and non-functional requirements for or formulate ICT systems to be adapted with the help of requirement models 
* I can check technologies and strategies for problem solving by means of feasibility studies and prototypes.
* I can define and review test objectives in collaboration with users and stakeholders; I can collaborate in creating test cases; I can plan, accompany and assess user tests; I can determine residual risks after the tests; I can validate and evaluate ICT solutions together with clients and stakeholders.


---
## ICT Security Specialist

Brief description  
Planning, evaluation, procurement, commissioning, testing and acceptance of suitable and cost-effective ICT security solutions based on the company-wide security strategy

* I can record, analyze, validate and document the requirements and expectations of the company and the system users with regard to ICT Security; I can develop solution proposals and decision bases based on the recorded requirements with regard to security, quality, cost-effectiveness as well as organizational and technical boundary conditions.
* I can investigate criminal activities in the context of ICT (groupings, targets, methods of attack) and their possible impact on the company; I can initiate appropriate protective measures.
* I can define, create and update security architectures and concepts.
* I can plan, design, install and integrate security components.
* I can analyze, describe, and document security vulnerabilities that occur during the
* I can check the degree to which the defined safety targets have been met.


---
## ICT System Engineer

Brief description  
Planning, procurement, commissioning, testing and acceptance of platforms (hardware, system software, networks, including cloud environments) for the operation of ICT systems; definition of the operational requirements

* I can examine new or changed requirements for their feasibility and complexity as well as their effects on existing / planned systems, standards and guidelines (requirement engineering).
* I can specify the system architecture; I can select hardware and system software as well as network and control components; I can search for possible alternatives and verify the feasibility; I can define the required technical and human resources.
* I can plan, evaluate, migrate, develop/implement, configure and test systems (hardware, system software, networks, control systems, including cloud environments) as well as put systems into productive operation.
* I can analyse malfunctions and error conditions; I can initiate and monitor troubleshooting measures
* I can maintain the technical system documentation (safety parameterization, architectural description, feasibility analysis, requirements analysis, etc.).


---
## ICT Test Engineer

Brief description  
Develop, specify, manage and execute tests with appropriate testing tools and techniques and in accordance with agreed process standards, industry regulations and based on the test strategy and test base; document the entire testing process

* I can create test specifications based on a detailed technical analysis for both functional and non-functional requirements (reliability, efficiency, usability, etc.).
* I can create test models using appropriate (test) techniques
* I can analyze the product risk analysis and determine the depth of testing; I can determine the residual risks after running the tests
* I can implement test strategies by applying standardized test methods and tools; I can create the test coverage matrix
* I can configure, update, maintain and deploy test tools


---
## ICT Test Manager

Brief description  
Defining and communicating the test policy and processes; creating, monitoring and adjusting the test strategy, test plans and test concepts; coordinating and monitoring the defined test activities in terms of quality, timing and cost

* I can create test policies, strategies, plans, methods and processes taking into account norms and standards.
* I can provide and review the necessary testing resources (personnel, methods/processes and tools)
* I can set up and maintain appropriate configuration management for the test equipment to track the tests performed
* I can perform deviation and bottleneck analyses (deviations from the test-concept, -plan and -script) and take measures to remedy the situation.
* I can introduce appropriate metrics to measure test progress and evaluate the quality of testing and the product


---
## Network Specialist

Brief description  
Plan, evaluate and implement appropriate and cost-effective network infrastructures and services

* I can understand the needs and expectations of the company and the users of the communication networks; I can develop proposals for solutions and bases for decisions based on the needs and expectations of the company and the users of the communication networks.
* I can define and create network architectures and concepts
* I can carry out evaluations to procure suitable network components, plan, design and install networks
* I can establish and enforce the security and surveillance criteria
* I can assess the security threats and the effectiveness of the security measures taken; if necessary, I can initiate appropriate measures to improve security


---
## Software Engineer

Brief description  
Development of software for hardware-related systems in control engineering, robotics and in the field of Internet of Things (IOT) as well as ensuring their implementation and maintenance; creation of complex algorithms (e.g. for parallel or distributed processes in time-critical systems)

* I can examine new or changed requirements for their feasibility and complexity as well as their impact on existing or planned systems, determine standards and guidelines (requirement engineering)
* I can define the necessary technical and human resources, as well as the requirements of the software architectures and the interfaces to the system environment (surrounding systems).
* I can create or select appropriate methods, techniques, standards, guidelines and tools for software development.
* I can develop software and software components as well as any interfaces to databases and to the system environment (surrounding systems).
* I can examine and analyse software systems with regard to security risks; I can initiate and implement the necessary measures identified business computer scientist


---
## ICT User Experience Architect

Brief description  
Analysis of user groups, their needs, expectations and usage contexts; conception, development and testing of user interfaces taking into account usage contexts and ergonomics as well as form factors, modalities and interaction patterns

* I can create interaction concepts and goals; I can define, create and document the design principles (design, development, style and platform guidelines); I can establish a usability engineering process with the necessary framework conditions.
* I can create, select and maintain suitable methods, techniques, standards, guidelines, tools, design libraries, modalities and interaction specifications for the development of user interfaces.
* I can create models and prototypes for user interaction and visual interface design in compliance with operational design guidelines, corporate identity, design laws and insights from perception psychology; I can develop, present and validate solution proposals and decision bases (conception, user-oriented design of interfaces and prototype).
* I can evaluate the feasibility under the aspects of ergonomics, safety, functionality,
* Verify quality, performance, context of use and cost-effectiveness; I can examine possible solution alternatives.
* I can plan and carry out usability tests (user testing) during development using methods for analyzing usage contexts; I can check the degree of fulfillment (summative usability evaluations) of the required requirements and carry out conformity tests (e.g. usability, accessibility).


---
## IS Engineer (IT Solution Engineer)

Brief description  
Designing, realizing, further developing, evaluating and procuring ICT solutions under the aspects: Profitability, security and feasibility; planning and accompanying the introduction of ICT solutions

* I can analyze problems related to business processes/transactions, data and functions; I can define requirements for the development or procurement of ICT solutions and identify possibilities for their implementation
* I can understand the needs and expectations of clients with regard to ICT solutions; I can develop solution proposals and decision bases with regard to security, functionality, quality and cost-effectiveness.
* I can create and monitor the overall planning of ICT projects with expenses, deadlines, human resources, resources and methods; I can regulate reporting and reporting
* I can ensure the consultation, support and instruction of system users during the implementation phase.
* I can include development trends in the field of information and communication technologies in the development of lCT solutions.


---
## Application Manager

Brief description  
Ensure the productive operation and maintenance of the applications used to support the business processes; ensure that the agreed operational requirements of the applications are met

* I can ensure the monitoring of the ongoing application operation; I can coordinate and monitor all activities that serve to track the availability, recoverability (continuity), performance and resource consumption of the applications
* I can handle application malfunctions/errors (analysis, workaround, fix) caused by application user or messages
* I can perform routine data cleansing and data maintenance functions in theExecute customer order
* I can create, update, maintain and manage the documentation required for application operation (operating manual, user manual, instructions, etc.).
* I can coordinate the further development of applications (recording of technical requirements, creation of solution concepts, preparation of quotations/release agreements).


---
## Database Administrator

Brief description  
Planning and processing of all database operation tasks as well as implementation of system-technical database concepts; implementation, maintenance, monitoring and servicing of databases including cross-platform and distributed databases

* I can implement, maintain and operate databases, including cross-platform and distributed databases.
* I can perform database monitoring (processes, objects, and reports).
* I can manage database objects (tablespaces, tables, indexes etc.)
* I can prepare and perform backup, reorganization and restart procedures for databases
* I can perform repair actions on faulty databases


---
## ICT Change Manager

Brief description  
Design, planning and implementation of measures to be implemented in the area of configuration/change management and their processes; introduction and maintenance of the required methods, standards, systems and tools

* I can plan, design and implement configuration and change management processes.
* I can evaluate, procure, recommend and implement configuration/change management systems and tools.
* I can define and implement measures in the area of configuration and change management as well as their methods and standards.
* I can process and implement change requests; I can coordinate changes during creation, testing and transfer to productive operation
* I can perform configuration audits and change reviews


---
## ICT Helpdesk Employee

Brief description  
Receiving questions and fault reports of all kinds; initiating escalation procedures if necessary; informing and supporting users in the event of operational faults and announcing planned system interruptions; remedying simple faults in the first instance or answering common questions or forwarding fault reports and questions to the competent bodies

* I can collect and classify necessary information
* I can pre-analyze fault reports and questions; I can correct simple faults (remote analysis); I can answer simple questions and arrange for any workarounds (first level support).
* I can forward questions and fault reports to the relevant authorities; I can track fault statistics
* I can inform the user about the proper use of the offered technical means and applications.
* I can release access permissions and, in exceptional situations, user IDs.


---
## ICT Operator

Brief description  
Operating and monitoring devices of a data processing system as well as subsystems (such as printing systems, dispatch lines and archiving systems)

* I can monitor and operate devices, consoles and subsystems and I can perform processing jobs including data backup.
* I can analyze system messages and initiate transitional and alternative measures in the event of malfunctions.
* I can clarify, document and eliminate faulty system states and exceptional situations.
* I can arrange and control necessary maintenance and servicing work
* I can lead logs to processing, maintenance and downtime, as well as for statistics and control records


---
## ICT Production Planner

Brief description  
Planning, preparation and monitoring of production

* I can maintain, build and prepare job nets for dialog and batch production (including recovery/restart).
* I can manage production schedules as well as schedule and take over new applications in production.
* I can advise the system controlling in case of problems with applications as well as manage automated data carrier archives.
* I can initiate tuning and performance measures
* I can define, develop and implement quality improvement measures (QA) and escalation procedures.


---
## ICT-Security-Operations-Manager

Brief description  
Operating, monitoring and maintaining company-wide security systems; analysing security-related events; initiating and monitoring measures to eliminate hazards; monitoring current threat situations and examining any effects on the company

* I can detect security incidents and their possible consequences on ICT systems (hardware, software), communication networks and services.
* I can coordinate the defense respectively elimination of security threatening events with Security Managers and Specialists of the company as well as with ICT Security suppliers and external service providers.
* I can develop tools to supplement the security systems used and to identify security-relevant events more efficiently.
* I can check security systems and tools in productive operation and the existing system environment (surrounding systems); I can set up logging procedures and interfaces to the existing system environment.
* I can assess the effectiveness of the security measures taken; if necessary, I can initiate further measures to improve security.


---
## ICT Service Manager

Brief description  
Negotiating and concluding service and operation level agreements; creating and implementing service and operation level documents and processes; introducing and maintaining the required standards, methods and tools; defining and implementing availability and performance management processes, standards, methods and tools

* I can define, coordinate and implement service / operation level management structures in terms of organization, policies, methods and standards.
* I can evaluate, recommend and implement service / operation management systems and tools. 
* I can evaluate, recommend and implement availability and performance management systems and tools. 
* I can check the agreed SLAs and OLAs (monitoring of services, availability, etc.) as well as the performance of systems, networks and applications.
* I can conduct service/operational management audits and create management reports


---
## ICT Supporter

Brief description  
Installing, maintaining and servicing workstation, peripheral, server and communication systems and their software; troubleshooting; advising, supervising, instructing and supporting users in the event of problems and problem solutions that arise

* I can implement according to installation and configuration specifications, directory structure, menu system, user interface, data backup and recovery.
* I can receive and handle hardware, software, network and communication issues.
* I can locate, isolate and correct faults or initiate an escalation procedure.
* I can develop simple scripts and applications for workstation systems and mobile devices based on standard software or universal application software for single users
* I can solve interface and conversion problems with both standard and universal application software.


---
## ICT System Administrator

Brief description  
Operating ICT services for operations; controlling, monitoring and ensuring the operation of data processing and server systems

* I can operate, control, monitor and maintain ICT services for operational use
* I can locate, isolate and resolve faults; I can initiate technical evasion and/or escalation procedures; I can initiate necessary recovery, restart, or other procedures and trigger restore processes
* I can take on and check orders within the framework of input-output management in cooperation with the customer.
* I can control and monitor system utilization (including tuning and performance measures)
* I can ensure information security when providing ICT services due to agreed service levels and defined security profiles; I can carry out or monitor the data backup.


---
## ICT System Controller

Brief description  
Operating, controlling and monitoring ICT systems; performing all operational processes and procedures; ensuring that all ICT services and ICT infrastructure are operated in accordance with the Service Level Agreements (SLAs)

* I can organize day-to-day management and perform all operational processes and procedures
* I can prepare, initiate, execute and monitor production processing;
* I can locate and isolate disruptions; I can initiate fallback and escalation procedures in third level support or with the supplier; I can trigger and execute necessary recovery, restart and restore procedures.
* I can document and cope with exceptional situations and organize technical transition and avoidance measures.
* I can initiate and supervise necessary maintenance and servicing work as well as system security measures


---
## ICT System Specialist

Brief description  
Processing of tasks on various technical platforms in the areas of hardware, operating, standard, universal and communication software

* I can install, generate, customize, integrate, test and release new hardware, operating universal and standard software.
* I can investigate the impact of new ICT components on existing systems; I can make recommendations taking into account the overall system
* I can define and/or create system security measures and checklists for change and problem management as well as define and create regulations for data security.
* I can realize concepts for tuning and performance tasks as well as determine and fix/bridge error states of hardware, software and communication components.
* I can handle complex reconstruction and restart procedures (Recovery/Execute Restart)


---
## ICT Technician

Brief description  
Installation and maintenance of stationary and mobile workstation, peripheral and communication systems as well as maintenance of common server systems and networks

* I can provision, install and maintain hardware, software and network components as well as communications equipment and peripherals.
* I can dislocate and expand workstation systems
* I can maintain and expand small local networks and install hardware and software.
* I can locate and fix faults in hardware, software and communication networks.
* I can keep a technical inventory of all installed and stored hardware and software components as well as all network components and licenses.


---
## Network Administrator

Brief description  
Operation, control and monitoring of communications networks; provision and maintenance of data, voice and multimedia services; ensuring the proper functioning of the communications network and initiating measures to remedy malfunctions

* I can operate communication networks and services; I can monitor the functionality and capacity of networks and network services; I can provide availability statistics
* I can assign and administer access rights, identifiers, addresses and directories for network operation and network services.
* I can locate and correct faulty conditions in networks and services; I can initiate measures to minimize susceptibility to faults and improve service quality
* I can take technical transitional and alternative measures in the event of failure of
* Initiating Network Components and Network Services
* I can perform performance and capacity analysis to monitor network capacity and utilization


---
## Project Management Officer

Brief description  
Supporting the project/program/portfolio management in all planning, recording, monitoring and information tasks; supporting the application of the cross-project project management system

* I can ensure the consistent and sustainable use of the project management system in projects
* I can manage guidelines, templates and tools for the execution of projects and programs as well as project leaders and project staff provide support in the use of these tools.
* I can manage project proposals and project changes; I can make recommendations for prioritizing them 
* I can mutate project plans; I can collaborate in managing delivery objects, deadlines, costs and project risks; I can inform project/program managers of anomalies and make proposals to correct deviations
* I can carry out the project-related quality and resource management


---
## Project Leader

Brief description  
Managing projects; ensuring the delivery of services with regard to delivery objects, deadlines, costs and risks; establishing the project organization and leading the project team

* I can analyze and concretize project ideas; I can assess project ideas in terms of feasibility, economic efficiency, risks and success factors in consultation with the portfolio, program and overall project management
* I can work out a project proposal with objectives, resources, milestones, schedule, costs, expenses, process model, project structure plan, risk plan and project organization; I can transfer the project proposal into a project proposal agreed with the client (including approval)
* I can understand the impact of change requests, problems, and changed requirements, recognizing the project's framework conditions; I can work out solution proposals in coordination with the client in case of project deviations; I can define the project marketing and the communication plan; I can ensure the project information; I can involve the stakeholders in the project work (maintaining relationships) 
* I can define the indicators for progress and success control; I can select suitable processes for measuring and evaluating the indicators; I can use controlling tools to monitor and influence the behavior of project team members and external suppliers.
* I can check and accept the project results; I can arrange for acceptance and approval by the client


---
## Program Manager

Brief description  
Planning, initiating and managing programmes (a set of several related projects with the same or similar objectives) taking into account the company strategy; prioritising and coordinating the individual projects of a programme; ensuring the necessary human resources in the area of project management

* I can combine projects into programs
* I can prioritize projects for the programs in collaboration with the responsible business units, taking into account the relevant strategies and framework conditions; I can prepare and bring about the necessary decisions of decision-makers
* I can create cross-project planning; I can manage risks and opportunities   
* I can define standards and guidelines; I can evaluate and implement appropriate methods and tools; I can enforce compliance with program-wide standards as well as compliance with guidelines and appropriate use of methods and tools. 
* I can evaluate verified project information; I can identify and document interdependencies and overlaps of resources (costs, personnel, system boundaries).


---
## Business Analyst

Brief description  
Identification, analysis, identification and communication of weak points in business processes, organisational structures, IT and material resources; elaboration of requirements for the realisation of operational solutions

* I can support the departments in the creation of requirement specifications and solution approaches taking into account the technical system requirements.
* I can analyze the overall context of business processes, organizational structures and IT as well as the use of material resources and the identification of weak points.
* I can collect, review and document the professional needs and expectations of organizational units in their project plans; I can develop proposals for solutions and decision bases based on the requirements recorded.
* I can process change requests from clients and project committees; I can develop the basis for decision-making for further action and prepare the quality assurance measures in accordance with the specified requirements.
* I can evaluate and validate operational solutions based on project specifications


---
## Process Manager

Brief description  
Developing and maintaining process models, process maps, business processes and workflows in the company; supporting management, the specialist departments as well as project and program managers in process management issues

* I can support management in developing process maps; I can prioritize and classify processes into core, leadership and support processes
* I can support and accompany workflow implementations and processes
* I can evaluate suitable tools for process orientation in the company and support employees in the introduction and training of these tools.
* I can identify weaknesses and their causes in business processes; I can optimize business processes on a conceptual level
* I can define process performance indicator systems and develop proposals for target measurement values.


---
## Business Organization Consultat

Brief description  
Erarbeiten von fachspezifischen organisatorischen Lösungen; Bearbeiten von betrieblichen und organisatorischen Problemstellungen; Gestalten betrieblicher Prozesse nach betriebswirtschaftlichen Grundsätzen

* I can support the project and program management and the management in ensuring quality, risk and cost management.
* I can allow, integrate and maintain the required systems, methods and tools.
* I can collect, analyze, evaluate and visualize business processes, structures and material resources.
* I can develop solutions for a sustainable optimization of operational tasks and accompany the implementation of solutions.
* I can work out, visualize and present solution proposals for the decision-making bodies


---
## Organization Manager

Brief description  
Advice and support of the company with regard to strategy, organization, use of resources, project management and business administration; development of complex project procedure methods and implementation of solutions

* I can analyze and evaluate the organizational structure based on the strategic corporate objectives
* I can create concepts for the management in the areas of innovation, risk management, Corporate-responsibility management and strategy development; I can accompany and implement these concepts
* I can define the process organization taking into account the corporate strategy, structure and culture.
* I can evaluate and implement methods and tools for organizational development
* I can create project, process and strategy reports for management


---
## Scrum Master

Brief description  
Ensure that the Scrum methodology is applied correctly. Lead Scrum teams based on agile principles towards self-organization. Promote optimal team collaboration and continuous product improvement with the goal of maximizing the value of ICT systems.

* Ensure that the Scrum events such as Daily Scrum, Retro, Sprint Planning etc. take place.
* Solving problems (so-called impediments) so that the Scrum team is not hindered in their work
* Supporting stakeholders in understanding and implementing the Scrum methodology and empirical product development
* Creating an understanding of product planning in the empirical work environment
* Guiding and coaching stakeholders in the introduction of the Scrum methodology
* Plan Scrum implementations and improve their effectiveness
* Ensure that product goals, scope and domain are known and understood within the Scrum team
* Promoting self-organization and cross-functional work in Scrum teams
* Supporting the Scrum team in the development of high-quality products
* To teach the techniques for effectively managing the product backlog Awakening understanding of the need for clear documentation of the product backlog through concise entries.
* Prioritize the entries in the product backlog in such a way that the largest value is generated.
* Collaborating in the organization and implementation of events for the Scrum methodology


---
## DevOps Engineer

Brief description  
Developing, installing, operating and maintaining software applications in an agile environment. Ensure compliance with agreed and required SLAs (quality, resources/costs, operations).

* Analyzing problems (business processes/transactions, data, functions) and the environment (workplace, target systems/platforms, framework organization)
* Processing (workaround, correction) of malfunctions and error states (business processes/transactions, data, functions) in the technical environment (workstation, target systems/platforms, networks)
* Defining functional and non-functional requirements (availability, safety, performance) as well as creating concepts and cost estimates
* Creating models for data, functions and objects
* Develop, implement and maintain software applications, components, modules or interfaces to databases and other applications.
* Designing, setting up and maintaining the development, test and production systems required for the software applications, including networks
* Automate processes for generating builds and releases, performing interface testing, regression testing, and software delivery (continuous delivery, continuous integration, build pipeline, code management, etc.)
* Coordinate all activities that serve to monitor application availability, recoverability (continuity), performance, and resource consumption.
* Coaching and guidance of team members in the "DevOps" area
* Creation and updating of documentation (models, interface descriptions, test procedures, user manuals, etc.)


---
## Product Owner

Brief description  
Responsible for effective ICT product development and maximizing the value of ICT systems for stakeholders in an agile environment.

* Establishing product thinking instead of project thinking in organizations
* Responsible for ICT product development
* Determination of product marketing (naming and branding)
* Analyzing the ICT market and the competition
* Operating an active stakeholder management system
* Identifying and analyzing customer needs
* Recording and documenting requirements
* Creating Business Cases
* Developing and implementing the product strategy
* Creation and maintenance of product roadmaps
* Coordinate product releases
* Maximizing the added value of ICT systems using the Scrum methodology
* Supporting the Scrum team in understanding the product visions
* Supporting the Scrum team in the selection of new, targeted technologies
* Defining Features (A/B Testing)
* Ensuring Governance and Audits
