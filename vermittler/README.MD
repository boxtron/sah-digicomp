<!---
Author: Daniel Boxhammer
-->

## Vermittler / Rekruter [^1]  
[<img src="/pic/payroll_oneagency.png" width="200" />](https://www.oneagency.ch/)


## Online Plattformen [^1]  
[<img src="/pic/payroll_honeypot.png" width="200" />](https://www.honeypot.io/de/)
[<img src="/pic/payroll_jobserve.png" width="200" />](https://www.jobserve.com/ch/en/Job-Search/)
[<img src="/pic/payroll_fiverr.png" width="200" />](https://de.fiverr.com/)  
[<img src="/pic/payroll_swissDevJobs.png" width="200" />](https://swissdevjobs.ch/de)
[<img src="/pic/payroll_itjobs.png" width="200" />](https://www.itjobs.ch/)
[<img src="/pic/payroll_freelancermap.png" width="200" />](https://www.freelancermap.ch/)

## andere Informationen [^1]  
[<img src="/pic/payroll_ictprognosse2024.png" width="200" />](./Studie_ICT-Bildungsbedarf_2024.pdf)
[<img src="/pic/payroll_ictlohnbuch2019.png" width="200" />](./it-markt.ch-So_viel_verdienen_Schweizer_Informatiker_2019.pdf)
[<img src="/pic/payroll_salarium.png" width="200" />](https://www.salarium.bfs.admin.ch/)  
[<img src="/pic/payroll_loopings.png" width="200" />](https://loopings.ch/)
[<img src="/pic/payroll_seniorworks.png" width="200" />](https://www.seniorsatwork.ch/)
[<img src="/pic/payroll_levels.jpg" width="200" />](https://www.levels.fyi/?compare=Google,Facebook,IBM&track=Software%20Engineer)

## PayRoller [^1]  
[<img src="/pic/payroll_Plus.png" width="200" />](https://payrollplus.ch/)
[<img src="/pic/payroll_helvetic.png" width="200" />](https://www.helvetic-payroll.ch/de/)
[<img src="/pic/payroll_payflow.png" width="200" />](https://www.payflow.ch/)

## Start-Up [^1]  
[<img src="/pic/payroll_startup.png" width="200" />](https://www.startup.ch/)  

## Bewertungs-Portale (Firmen) [^1]  
[<img src="/pic/payroll_kununu.png" width="200" />](https://www.kununu.com/ch)

[^1]: (Liste nicht vollständig)  
