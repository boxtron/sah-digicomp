<!---
Author: Daniel Boxhammer
-->

![Bild 1](./pic/signet.jpg "Signet")


# ICT Standortbestimmung (D) | Allgemeine Informationen

* [Ausbildung | Weiterbildung](ausbildung/)  
* [Kompetenzen | Fähigkeiten](kompetenzen/)  
* [Vermittler | Rekruter | Payroller](vermittler/)  
* [Tools](tools/)
* [Info](info/)  

# ICT Proficiency Assessment (E) | General Information
* [Job-Roles ICT](english/en_berufe_der_ict.md)
* [language school](https://www.flyingteachers.ch/de/allgemeine-sprachkurse/kurssuche.html)
* [Info](info/)  

---
[<img src="./pic/swissICT-berufe_ict.png" width="200" border="" />](https://www.berufe-der-ict.ch/berufe)  

