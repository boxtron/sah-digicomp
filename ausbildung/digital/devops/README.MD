<!---
Author: Daniel Boxhammer
-->

## DevOps Engineer | Spezialisierung (D), specialization (E) [^1]
[<img src="/pic/digital_udacity.png" width="200" />](https://www.udacity.com/course/data-scientist-nanodegree--nd025)
[<img src="/pic/digital_DevOpsEng1.png" width="200" />](https://www.simplilearn.com/devops-engineer-masters-program-certification-training)
[<img src="/pic/digital_cka.png" width="200" />](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/)
[<img src="/pic/digital_sit.png" width="200" />](https://sit.academy/de/remote/devops)  
[<img src="/pic/digital_DevOps_HSLU.png" width="200" />](https://www.hslu.ch/de-ch/informatik/weiterbildung/technologies-and-methods/cas-devops-and-cloud-transformation/?gclid=CjwKCAjwhJukBhBPEiwAniIcNYw7bCqn6SL0cAjFvEgHpjmUKoY_yLpUyG9VWjU_6i3wY3kVhHBl9hoC5XYQAvD_BwE&gclsrc=aw.ds)
[<img src="/pic/digital_cas-adv-Cloud.png" width="200" />](https://www.ffhs.ch/de/weiterbildung/cas-advanced-cloud-computing)
[<img src="/pic/digital_CAS-HWZ-Platform.png" width="200" />](https://fh-hwz.ch/bildungsangebot/weiterbildung/cas-das/cas-platforms-and-ecosystems-hwz?utm_term=&utm_campaign=(p_weiterbildung-cas)(g_commitment)(o_conversions)(i_num)(n_gsn)(t_generic)(f_std-gsn)&utm_source=adwords&utm_medium=ppc&hsa_acc=7787536494&hsa_cam=15913962204&hsa_grp=139091417437&hsa_ad=680715063485&hsa_src=g&hsa_tgt=dsa-2313994908917&hsa_kw=&hsa_mt=&hsa_net=adwords&hsa_ver=3&gad_source=1&gclid=Cj0KCQiA4Y-sBhC6ARIsAGXF1g62ovbe2RxgyohuZmXPZnz7bjhpCPPUaMi-4dguZh5eujFGYEBo8xEaAul1EALw_wcB)
[<img src="/pic/digital_CAS Cloud and Platform Manager.png" width="200" />](https://www.hslu.ch/de-ch/informatik/weiterbildung/technologies-and-methods/cas-cloud/?gclid=Cj0KCQjw4bipBhCyARIsAFsieCxNFaIh_7YBCd8THRIOexKqr9MLRVxLiACGiBK9S4bEzBtTrM4xUPQaAh0YEALw_wcB&gclsrc=aw.ds)  
[<img src="/pic/digital_ICT-Platform Development Specialist.png" width="200" />](https://www.ict-berufsbildung.ch/weiterbildung/fachausweis/ict-platform-development-specialist?gad_source=1&gclid=CjwKCAjwps-zBhAiEiwALwsVYbYSNmuRrHdd5A11n82tEfUZfMbohAnxai7sDFg1LYlqDInlvVytbhoC6eUQAvD_BwE)


### NANODEGREE PROGRAMS (UdaCity)
* [Hybrid Cloud Engineer](https://www.udacity.com/course/hybrid-cloud-engineer-nanodegree--nd321)
* [Cloud Developer using Microsoft Azure](https://www.udacity.com/course/cloud-developer-using-microsoft-azure-nanodegree--nd081)
* [Cloud Devops using Microsoft Azure](https://www.udacity.com/course/cloud-devops-using-microsoft-azure-nanodegree--nd082)
* [AWS Cloud Architect](https://www.udacity.com/course/aws-cloud-architect-nanodegree--nd063)
* [Cloud Developer](https://www.udacity.com/course/cloud-developer-nanodegree--nd9990)
* [Cloud DevOps](https://www.udacity.com/course/cloud-dev-ops-nanodegree--nd9991)
* [Cloud Computing for Business Leaders](https://www.udacity.com/course/cloud-computing-for-business-leaders-nanodegree--nd046)
* [Cloud Native Application Architecture](https://www.udacity.com/course/cloud-native-application-architecture-nanodegree--nd064)

[^1]: (Liste nicht vollständig)  
