# [Berufsabschluss für Erwachsene](http://www.berufsbildung.ch/dyn/9640.aspx)

## Validierungsverfahren in fünf Phasen
Das Verfahren zur Validierung von Bildungsleistungen hat fünf Phasen.

![Bild 1](validierungsverfahren.jpg "Grafische Darstellung des Validierungsverfahrens in fünf Phasen")  


### Phase 1: Information und Beratung (Eingangsportal)
Interessierte erhalten im Eingangsportal ihres Wohnkantons Information und persönliche Beratung. Dazu gehören allgemeine Informationen über das Verfahren, spezifische Informationen über einzelne Berufe und Unterstützung beim Erstellen des Dossiers.

Zuständig sind:
* Die Staatssekretariat für Bildung, Forschung und Innovation SBFI: Informationsverbreitung für Fachleute in Kantonen und Organisationen der Arbeitswelt
* Kantone: Betreuung des Eingangsportals (= Beratungsstelle), Informationsverbreitung
* Schweizerisches Dienstleistungszentrum Berufsbildung | Berufs-, Studien- und Laufbahnberatung SDBB: berufsspezifische Informationsaufbereitung, Informationsverbreitung
* OdA: Informationsverbreitung

### Phase 2: Bilanzierung
Die Bilanzierung erlaubt einer Person, ihre persönlichen Kompetenzen und ihre beruflichen Handlungskompetenzen zu identifizieren, zu analysieren und zu dokumentieren. Das entsprechende Dossier kann selbstständig oder mit Unterstützung von Fachleuten zusammengestellt werden.

Zuständig sind:
* Kantone: Die zuständige Beratungsstelle gibt detaillierte schriftliche Informationen zur selbstständigen Zusammenstellung des Dossiers ab. Sie bietet für den Bilanzierungsprozess Begleitung oder Coaching an.
* Kandidat/in: Erstellen des Dossiers

### Phase 3: Beurteilung
Experten und Expertinnen prüfen das Dossier und beurteilen es anhand des Qualifikationsprofils und der Bestehensregeln für den angestrebten Titel.
Das Ergebnis wird in einem Bericht zuhanden des Validierungsorgans festgehalten.

Zuständig sind:
* Experten/Expertinnen: Studium des Dossiers, Gespräch mit dem Kandidaten oder der Kandidatin, allenfalls zusätzliche Überprüfungen, Verfassen des Beurteilungsberichts
* Kantone: Die Prüfungsleitung wählt die Experten und Expertinnen, übermittelt das Dossier, erstellt den Zeitplan und organisiert konkrete Beurteilungsschritte.

### Phase 4: Validierung
Ein kantonales oder regionales Validierungsorgan entscheidet, welche Bereiche des Qualifikationsprofils erfüllt sind. Ist die nötige berufliche Handlungskompetenz in einem Bereich vorhanden, muss der Kandidat oder die Kandidatin dafür keine weiteren Nachweise oder Prüfungen mehr erbringen (= Teilzertifizierung). Das Resultat wird in einer Lernleistungsbestätigung festgehalten. Gleichzeitig wird angegeben, welche Kompetenzen noch fehlen und wie sie erworben werden können.

Die Kandidierenden sollten die fehlenden beruflichen Handlungskompetenzen grundsätzlich in strukturierten Kursen mit Prüfungen oder durch weitere berufspraktische Erfahrungen erwerben. Dies hat innerhalb von fünf Jahren zu geschehen.

Zuständig sind:
* Validierungsorgan: Entscheid über die anzurechnenden Handlungskompetenzbereiche
* Kantone: Die Prüfungsbehörde teilt den Anrechnungsentscheid und die Frist für die Einreichung weiterer Nachweise mit.
* Kantone und OdA: Aufbau von Angeboten zur ergänzenden Bildung

### Phase 5: Zertifizierung
Die Prüfungsbehörde überprüft die Nachweise von Bildungsleistungen. Es gibt drei mögliche Arten von Nachweisen: Gleichwertigkeitsbescheinigungen aus früheren Bildungsgängen, Lernleistungsbestätigungen aus dem Validierungsverfahren, Prüfungsprotokolle aus der ergänzenden Bildung.

Zuständig sind:
* Kantone: Kontrolle und Erwahrung der Bildungsnachweise, Ausstellung des Titels und des Ausweises.

