<!---
Author: Daniel Boxhammer
-->

# Programme des Bundes / der Kantone  


[Link zu NQR](http://www.nqr-berufsbildung.ch)  

![Bild 1](/pic/edu_bildungssystem_ch_simple.PNG "Signet")  

* [Bildungssystem Übersicht)](https://www.berufsberatung.ch/dyn/show/2800) 

## Stufe EFZ (#4)  

* [Validierungsverfahren in den Kantonen](https://www.berufsberatung.ch/dyn/show/26870)
* [Beispiel Kanton ZH](https://www.zh.ch/de/bildung/berufs-studien-laufbahnberatung/laufbahnberatung/bildungsabschluss-nachholen.html#-1667651594)  
  [<img src="/pic/edu_zhamtberuf.png" width="200" />](https://ajb.zh.ch/internet/bildungsdirektion/ajb/de/berufsberatung/standorte/biz-oerlikon/fachstelle-berufsabschluss-fuer-erwachsene/validierung.html)  
* [Ausbildungszuschüsse bei Erstausbildung](./AZ_Info_für_Stes.pdf)
* [Validierungsverfahrens in fünf Phasen](./validierung.md)
  
### Ausbildungen EFZ (inkl. Bildungspläne)  
* [Applikations-Entwickler*In](https://www.ict-berufsbildung.ch/resources/Bildungsplan_Informatik-EFZ_Applikationsentwicklung_BiVo-2014_DE1.pdf)
* [Betriebs-Informatiker*In](https://www.ict-berufsbildung.ch/resources/Bildungsplan_Informatik-EFZ_Betriebsinformatik_BiVo-2014_DE1.pdf)
* [Systemtechniker*In](https://www.ict-berufsbildung.ch/resources/Bildungsplan_Informatik-EFZ_Systemtechnik_BiVo-2014_DE.pdf)
* [ICT-Fachmann/-Fachfrau](https://www.ict-berufsbildung.ch/grundbildung/ict-lehren/ict-fachmann-frau-efz)


## Stufe HF / FH (#6)  

[<img src="/pic/edu_informa.png" width="200" />](https://www.informa-modellf.ch/)  


### Diverse Links  
----
* [Bundesbeiträge für Kurse, die auf eidgenössische Prüfungen vorbereiten](https://www.sbfi.admin.ch/sbfi/de/home/bildung/hbb/finanzierung.html)
* [Anerkennung ausländischer Diplome](https://www.sbfi.admin.ch/sbfi/de/home/bildung/diploma.html)
* [Niveaubescheinigung/Bewertung für ausländische Hochschuldiplome](https://www.swissuniversities.ch/service/anerkennung/swiss-enic)
* [Liste der Berufe mit NQR](https://www.fedlex.admin.ch/eli/cc/2015/269/de)
* [Der Nationale Qualifikationsrahmen (NQR) Berufsbildung](https://www.sbfi.admin.ch/sbfi/de/home/bildung/mobilitaet/nqr.html)
* [Länder- und Berufsprofile (Deutschland => Bundesministerium für Wirtschaft und Energie)](https://www.bq-portal.de/db/L%C3%A4nder-und-Berufsprofile)
* [Individuelle Kurse - Hilfe über das RAV](https://www.zh.ch/de/wirtschaft-arbeit/stellensuche-arbeitslosigkeit/qualifizierung-stellensuchende/individuelle-kurse.html#1290580186)
* [Englische Titelbezeichnungen für Abschlüsse der Berufsbildung](https://www.sbfi.admin.ch/sbfi/de/home/bildung/bwb/hbb/abgeschlossene-projekte/strategieprojekt-hbb/englische-titelbezeichnungen.html)