<!---
Author: Daniel Boxhammer
-->

## Anbieter Trainee-Programme | andere [^1]  
[<img src="/pic/trainee_career_cs.png" width="200" />](https://www.credit-suisse.com/careers/de/career-opportunities/students-and-graduates/full-time-programs/switzerland-career-start.html)
[<img src="/pic/trainee_programme.png" width="200" />](https://www.traineeprogramme.ch/)  

## Dokumente
[kib_trainee_praktikum_stage_dez_18.pdf](./12450-40660-3-kib_trainee_praktikum_stage_dez_18.pdf)

## Anbieter Praktikum | andere [^1]  
[<img src="/pic/trainee_learn-earn.png" width="200" />](https://ogc.winterthur.ch/directories/programme/learn-earn)

[^1]: (Liste nicht vollständig)  
